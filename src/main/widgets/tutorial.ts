import { Widget } from "prest-lib/dist/hsml-widget";
import { Hsmls } from "prest-lib/dist/hsml";

export class TutorialWidget extends Widget {
    constructor() {
        super("TutorialWidget");
    }

    onMount() {
        // console.log("onMount", this.type, this.id);
    }

    onUmount() {
        // console.log("onUmount", this.type, this.id);
    }

    render(): Hsmls {
        return [
            ["h2", ["Diabetes sheet"]],
            ["p", [
                "Web/Mobile Diabetes sheet application (PWA)"
            ]],
            ["h3", ["How to use it"]],
            ["div.w3-row.w3-stretch", [
                ["div.w3-col.m4.w3-panel", [
                    ["div.w3-card-4.w3-white", [
                        ["div.w3-padding", [
                            ["img.image",
                                {
                                    src:  "assets/images/login.png",
                                    alt: "login"
                                }
                            ]
                        ]],
                        ["div.w3-container", [
                            ["p",
                                ["Log in through your Google account."]
                            ]
                        ]]
                    ]]
                ]],
                ["div.w3-col.m4.w3-panel", [
                    ["div.w3-card-4.w3-white", [
                        ["div.w3-padding", [
                            ["img.image",
                                {
                                    src:  "assets/images/add_record.png",
                                    alt: "add record"
                                }
                            ]
                        ]],
                        ["div.w3-container", [
                            ["p",
                                ["Fill in the form whenever you want to add a new record. The date and time will be pre-filled for you, so you only need to add your blood sugar levels and the insulin dose you've taken."]
                            ]
                        ]]
                    ]]
                ]],
                ["div.w3-col.m4.w3-panel", [
                    ["div.w3-card-4.w3-white", [
                        ["div.w3-padding", [
                            ["img.image",
                                {
                                    src:  "assets/images/view_records.png",
                                    alt: "view records"
                                }
                            ]
                        ]],
                        ["div.w3-container", [
                            ["p",
                                ["View all your records at any time."]
                            ]
                        ]]
                    ]]
                ]]
            ]]
        ];
    }
}
