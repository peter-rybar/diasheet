import { Widget } from "prest-lib/dist/hsml-widget";
import { Hsmls } from "prest-lib/dist/hsml";

export class LoadingWidget extends Widget {
    constructor() {
        super("LoadingWidget");
    }

    onMount() {
        // console.log("onMount", this.type, this.id);
    }

    onUmount() {
        // console.log("onUmount", this.type, this.id);
    }

    render(): Hsmls {
        return [
            ["h2", ["Diabetes sheet"]],
            ["p", [
                "Web/Mobile Diabetes sheet application (PWA)"
            ]],
            ["p", [
                "Loading..."
            ]]
        ];
    }

}
