import { Widget } from "prest-lib/dist/hsml-widget";
import { Hsmls } from "prest-lib/dist/hsml";

export class AuthWidget extends Widget {
    private _onLogin: () => void;
    private _onMore: () => void;

    constructor() {
        super("AuthWidget");
    }

    onMount() {
        // console.log("onMount", this.type, this.id);
    }

    onUmount() {
        // console.log("onUmount", this.type, this.id);
    }

    setOnLogin = (handler: () => void): this => {
        this._onLogin = handler;
        return this;
    }

    onLogin = (): void => {
        this._onLogin && this._onLogin();
    }

    setOnMore = (handler: () => void): this => {
        this._onMore = handler;
        return this;
    }

    onMore = (): void => {
        this._onMore && this._onMore();
    }

    render(): Hsmls {
        return [
            ["h2", ["Diabetes sheet"]],
            ["p", [
                "Web/Mobile Diabetes sheet application (PWA)"
            ]],
            ["h3", ["What it is"]],
            ["div.w3-card", [
                ["img.image",
                    {
                        src:  "assets/images/img.png"
                    }
                ]
            ]],
            ["p", [
                "Diabetes sheet is a Blood Sugar Diary/Spreadsheet Progressive Web App which writes diabetic data to Google sheet for better data visualization and online sharing with your health care provider, supervising doctor, parents or friends."
            ]],
            ["p", [
                "Already familiar? Log in!"
            ]],
            ["p", [
                ["button.w3-btn.w3-blue", { click: this.onLogin }, ["Log in"]]
            ]],
            ["p", [
                "Want to know more?"
            ]],
            ["p", [
                ["button.w3-btn.w3-green", { click: this.onMore }, ["Explore"]]
            ]],
            ["footer", [
                ["p.w3-small", [
                    ["span", ["Icons made by "]],
                    ["a",
                        {
                            href: "https://www.freepik.com/",
                            target: "_blank",
                            title: "Freepik"
                        },
                        ["Freepik"]
                    ],
                    ["span", [" from "]],
                    ["a",
                        {
                            href: "https://www.flaticon.com/",
                            target: "_blank",
                            title: "Flaticon"
                        },
                        ["Flaticon"]
                    ],
                    ["span", [" licensed by "]],
                    ["a",
                        {
                            href: "http://creativecommons.org/licenses/by/3.0/",
                            target: "_blank",
                            title: "CC 3.0 BY"
                        },
                        ["CC 3.0 BY"]
                    ],
                ]]
            ]]
        ];
    }
}
