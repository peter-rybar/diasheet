import { swInit } from "./sw-lib";
import { AppShell } from "./appshell";
import { SidebarWidget } from "./sidebar";
import { Widget } from "prest-lib/dist/hsml-widget";
import { Hash } from "prest-lib/dist/hash";
import { AuthWidget } from "./widgets/auth";
import { ViewRecordsWidget } from "./widgets/view-records";
import { AddRecordWidget } from "./widgets/add-record";
import { GapiHandler } from "./gapi";
import { LoadingWidget } from "./widgets/loading";
import { GA } from "prest-lib/dist/google-analytics";
import { AboutWidget } from "./widgets/about";
import { TutorialWidget } from "./widgets/tutorial";

swInit();

const trackingId = location.hostname === "localhost" ? "" : "UA-134923731-1";
const ga = new GA(trackingId);

const loading = new LoadingWidget();
const auth = new AuthWidget();
const about = new AboutWidget();
const tutorial = new TutorialWidget();
const addRecord = new AddRecordWidget();
const viewRecords = new ViewRecordsWidget();
const sidebar =  new SidebarWidget();
const gapiHandler = new GapiHandler();

const app = new AppShell<SidebarWidget, Widget, GapiHandler>()
    .setTitle("DiaSheet")
    .setGapiHandler(gapiHandler)
    .setSidebar(sidebar)
    .setContent(loading)
    .mount(document.getElementById("app"));

auth.setOnLogin(() => app.getGapiHandler().signIn());
auth.setOnMore(() => route("about"));
sidebar.setOnClick(() => app.getGapiHandler().signOut());

const contents: { [key: string]: Widget } = {
    "auth": auth,
    "about": about,
    "tutorial": tutorial,
    "add-record": addRecord,
    "view-records": viewRecords
};

const route = (path: string) => {
    let routePath = path;
    const isSignedIn = app.getGapiHandler().getAuthStatus();

    if (!isSignedIn && (path === "add-record" || path === "view-records")) {
        routePath = "auth";
    }

    if (isSignedIn && path === "" || isSignedIn && path === "auth") {
        routePath = "view-records";
    }

    app.sidebarClose();
    sidebar.setHash(routePath);
    sidebar.setLoggedIn(isSignedIn);
    hash.write(routePath);
    ga.pageview(routePath);
    const content = contents[routePath];
    if (content) {
        app.setContent(content);
    } else {
        if (path) {
            hash.write("");
        } else {
            app.setContent(auth);
        }
    }
};

const hash = new Hash<string>()
    .coders(data => data,  str => str)
    .onChange(route);

enum Actions {
    LOAD_GAPI_SUCCESS = "LOAD_GAPI_SUCCESS",
    AUTH_STATUS_CHANGE = "AUTH_STATUS_CHANGE",
    CHECK_SHEET_EXISTS = "CHECK_SHEET_EXISTS",
    LOAD_SHEET = "LOAD_SHEET",
    UPDATE_SHEET_DATA = "UPDATE_SHEET_DATA",
    SUBMIT_SHEET_FORM = "SUBMIT_SHEET_FORM"
}

function dispatch(action: Actions, data?: any): void {
    switch (action) {
        case Actions.LOAD_GAPI_SUCCESS: {
            const isSignedIn = app.getGapiHandler().getAuthStatus();
            dispatch(Actions.AUTH_STATUS_CHANGE, isSignedIn);
            break;
        }
        case Actions.AUTH_STATUS_CHANGE: {
            if (data) {
                dispatch(Actions.CHECK_SHEET_EXISTS);
            }
            route(hash.read());
            break;
        }
        case Actions.CHECK_SHEET_EXISTS: {
            app.getGapiHandler().checkIfSheetExists()
                .then((sheet) => {
                    if (!sheet) {
                        app.getGapiHandler().createSheet()
                            .then((response: any) => {
                                const id = response.result.spreadsheetId;
                                viewRecords.setSheetUrl(id);
                                app.getGapiHandler().setSpreadSheetId(id);
                            })
                            .catch(() => alert("Error creating sheet"));
                    } else {
                        viewRecords.setSheetUrl(sheet.id);
                        app.getGapiHandler().setSpreadSheetId(sheet.id);
                        dispatch(Actions.LOAD_SHEET);
                    }
                })
                .catch(() => alert("Error reading Drive"));
            break;
        }
        case Actions.LOAD_SHEET: {
            app.getGapiHandler().getSheet()
                .then((response: any) => {
                    if (response.result && response.result.values) {
                        dispatch(Actions.UPDATE_SHEET_DATA, response.result.values);
                    }
                })
                .catch(() => alert("Error loading sheet"));
            break;
        }
        case Actions.UPDATE_SHEET_DATA: {
            viewRecords.setState({ data });
            break;
        }
        case Actions.SUBMIT_SHEET_FORM: {
            addRecord.setIsLoading(true);
            app.getGapiHandler().appendToSheet(data)
                .then(() => {
                    app.getGapiHandler().getSheet()
                        .then((response: any) => {
                            addRecord.setIsLoading(false);
                            if (response.result && response.result.values) {
                                route("view-records");
                                dispatch(Actions.UPDATE_SHEET_DATA, response.result.values);
                                addRecord.resetForm();
                            }
                        })
                        .catch(() => {
                            addRecord.setIsLoading(false);
                            alert("Error updating sheet");
                        });
                });
            break;
        }
        default:
            break;
    }
}

app.getGapiHandler().init()
    .then(() => {
        dispatch(Actions.LOAD_GAPI_SUCCESS);
        app.getGapiHandler().setAuthStatusListener((isSignedIn =>
            dispatch(Actions.AUTH_STATUS_CHANGE, isSignedIn))
        );
    })
    .catch(() => alert("Error initializing Google Api client"));
addRecord.onSave((data) => dispatch(Actions.SUBMIT_SHEET_FORM, data));
viewRecords.setOnClick(() => route("add-record"));

(self as any).app = app;

(self as any).VERSION = "@VERSION@";
