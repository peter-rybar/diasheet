# Diasheet

[Diabetes](https://en.wikipedia.org/wiki/Diabetes_mellitus) sheet is Food/Blood Sugar Diary/Spreadsheet [Progresive Web App](https://developers.google.com/web/progressive-web-apps/)
which writes diabetic data to [Google sheet](https://www.google.com/sheets/about/) for better data visualization and online sharing with supervising doctor, parents or friends.

## Diasheet app

[https://peter-rybar.gitlab.io/diasheet/](https://peter-rybar.gitlab.io/diasheet/)

## Roadmap

- Build minimal application functionality (done)
- Spread in community of diabetics for testing and Collect feedback
- Evaluate community feedback and implement requirements
- Parental and senior control support
- Suprevisoring doctor data analytics
- [eHealth](https://en.wikipedia.org/wiki/EHealth) system integration
- Add more valuable functionality base on comunity users or medical institutions.

## TODO

- zistit ake appky pouzivaju diabetici a preco
- logo - modry kruh
- Marketing
- Budovat komunitu
- Feedback pacienti a lekari
- Aka je spolocenska objednavka
- Migracia na Awidget
- Css a fonty lokalne
- Parental control
- Upload z glukomeru do googlu
